//
//  HomePageViewController.swift
//  AccountSignUp-IOS-Homework2
//
//  Created by Sophealey on 11/25/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit

class HomePageViewController: UIViewController {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    var userName:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        if let name = userName{
            userNameLabel.text = "Welcome " + name
        }else{
            userNameLabel.text = "Sign up failed!"
            messageLabel.text = "All field are required!"
        }
        
    }

}
