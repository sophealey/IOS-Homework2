//
//  ViewController.swift
//  AccountSignUp-IOS-Homework2
//
//  Created by Sophealey on 11/23/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,UITextFieldDelegate {

    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    let label = UILabel(frame: CGRect(x: 30, y: 40, width: 80 , height:30))
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameTextField.delegate=self
        passwordTextField.delegate=self
        phoneTextField.delegate=self
        emailTextField.delegate=self
        
        userNameTextField.borderStyle = .roundedRect
        passwordTextField.borderStyle = .roundedRect
        phoneTextField.borderStyle = .roundedRect
        emailTextField.borderStyle = .roundedRect
        
        phoneTextField.addTarget(self, action: #selector(phoneTextChange(_:)), for: UIControlEvents.editingChanged)
 
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //check phoneTextField BeginEditing
        if textField.tag == 3 {
            label.text = " (+855)"
            label.font = UIFont(name: label.font.fontName, size: 22)
            label.sizeToFit()
            phoneTextField.leftView = label
            phoneTextField.leftViewMode = UITextFieldViewMode.always
            phoneTextField.placeholder = "096-488-3800"
            
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 3 && (textField.text?.isEmpty)!{
            resetLeftView()
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1 as Int
        let nextField: UIResponder? = textField.superview?.viewWithTag(nextTag)
        if let field:UIResponder = nextField{
            field.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return false
    }
    
    private func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "XXX-XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask{
            if index == cleanPhoneNumber.endIndex {
                break
            }
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    @objc func phoneTextChange(_ sender: UITextField){
        //Format phone number
        phoneTextField.text = formattedNumber(number: sender.text!)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if validateText(){
            if segue.identifier == "sendName"{
                let mainPage = segue.destination as? HomePageViewController
                mainPage?.userName = userNameTextField.text?.uppercased()
                //reset form
                resetForm()
            }
        }else{
            print("Text required")
        }
        
    }
    
    //Reset Form
    func resetForm(){
        userNameTextField.text = ""
        passwordTextField.text = ""
        resetLeftView()  //reset left view of phoneTextField
        phoneTextField.text = ""
        emailTextField.text = ""
        
    }
    //Set left view of UITextField to blank
    func resetLeftView(){
        let blankLeftView = UILabel()
        phoneTextField.leftView = blankLeftView
        phoneTextField.placeholder = "(+855) 096-488-3800"
    }
    
    @IBAction func unwindSegue(_ sender: UIStoryboardSegue){
        print("this is unwinding")
    }

    func validateText()-> Bool{
        var result:Bool = true
        if userNameTextField.text == "" || (userNameTextField.text?.isEmpty)!{
            result = false
        }else if passwordTextField.text == "" || (passwordTextField.text?.isEmpty)!{
            result = false
        }else if phoneTextField.text == "" || (phoneTextField.text?.isEmpty)!{
            result = false
        }else if emailTextField.text == "" || (emailTextField.text?.isEmpty)! {
            result = false
        }
        return result
    }

}

